# React Template

This is a vanilla CRA React App (React v.17.0.1) with the following added:

- User Login
- Routing
  - Private Route
- Shell scripts that allow us to use .env

- State

  - Redux
  - Sagas

- Craco specifically to override the hooks issue when you npm link packages for testing.

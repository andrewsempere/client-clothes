import "./App.css";
import { Provider } from "react-redux";
import store from "./state/store";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import { Home, Private } from "./pages";
import * as User from "./pages/User";
import * as Users from "./pages/Users";
import PrivateRoute from "./router/PrivateRoute";
import GuestRoute from "./router/GuestRoute";

function App() {
  return (
    <Provider store={store}>
      <Router>
        <div className="App">
          {/*<div>
            <Navigation />
            <Status />
          </div>*/}
          <Switch>
            <PrivateRoute path="/private" component={Private} />{" "}
            <GuestRoute
              path="/account/validate/:token"
              component={User.Validation}
            />
            <Route path="/user/help" component={User.Help} />
            <PrivateRoute path="/user/delete" component={User.Delete} />
            <PrivateRoute
              requires={["websuper"]}
              path="/users"
              component={Users.Manage}
            />
            <GuestRoute path="/user/reset" component={User.Password} />
            <PrivateRoute path="/user/update" component={User.Update} />
            <Route exact path="/" component={Home} />
            <Route path="/" render={() => <Redirect to="/" />} />
          </Switch>
        </div>
      </Router>
    </Provider>
  );
}

export default App;

import * as THREE from "three";
import { FBXLoader } from "three/examples/jsm/loaders/FBXLoader.js";
import Stats from "three/examples/jsm/libs/stats.module.js";
import { OrbitControls } from "three/examples/jsm/controls/OrbitControls";

const fbxViewer = {
  sceneInit: (size, renderer, container, stats, cb) => {
    let { width, height } = size;
    const scene = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera(45, width / height, 1, 2000);
    camera.position.set(0, 120, 200);
    renderer = null;
    renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
    renderer.setClearColor(0x000000, 0);
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(width, height);
    renderer.shadowMap.enabled = true;
    container.innerHTML = "";
    container.appendChild(renderer.domElement);
    if (stats) {
      stats.current = new Stats();
      container.appendChild(stats.current.dom);
    }
    if (typeof cb == "function") cb({ renderer, scene, camera });
    return { renderer, scene, camera };
  },

  enableControls: (camera, renderer) => {
    const controls = new OrbitControls(camera, renderer.domElement);
    controls.target.set(0, 0, 0);
    controls.update();
    return controls;
  },

  sceneSetup: (scene) => {
    const grid = false;
    const groundPlane = false;
    const fog = false;

    // Fog
    if (fog) {
      scene.background = new THREE.Color(0xbbbbbb);
      scene.fog = new THREE.Fog(0xa0a0a0, 200, 1000);
    }

    // Ground
    if (groundPlane) {
      const groundPlane = new THREE.Mesh(
        new THREE.PlaneGeometry(2000, 2000),
        new THREE.MeshPhongMaterial({ color: 0x999999, depthWrite: false })
      );
      groundPlane.rotation.x = -Math.PI / 2;
      groundPlane.receiveShadow = true;
      scene.add(groundPlane);
    }

    // Grid
    if (grid) {
      const grid = new THREE.GridHelper(2000, 20, 0x000000, 0x000000);
      grid.material.opacity = 0.2;
      grid.material.transparent = true;
      scene.add(grid);
    }
  },

  lightScene: (scene) => {
    const hemiLight = new THREE.HemisphereLight(0xffffff, 0x444444);
    hemiLight.position.set(0, 200, 0);
    scene.add(hemiLight);

    const dirLight = new THREE.PointLight(0xffffff, 1, 140);
    dirLight.position.set(0, 200, 100);
    dirLight.castShadow = true;
    dirLight.shadow.camera.top = 180;
    dirLight.shadow.camera.bottom = -100;
    dirLight.shadow.camera.left = -120;
    dirLight.shadow.camera.right = 120;
    //dirLight.shadow.bias = -0.001;
    scene.add(dirLight);
    scene.add(new THREE.CameraHelper(dirLight.shadow.camera));
  },

  swapMaterial: (materialUrl, item) => {
    var textureLoader = new THREE.TextureLoader();
    textureLoader.setCrossOrigin("anonymous");
    textureLoader.load(`${materialUrl}/normal.png`, (normal) => {
      textureLoader.load(`${materialUrl}/roughness.png`, (roughness) => {
        textureLoader.load(`${materialUrl}/base_color.png`, (texture) => {
          item.material.side = THREE.DoubleSide;
          item.material.normalMap = normal;
          item.material.roughnessMap = roughness;
          item.material.map = texture;
          item.material.needsUpdate = true;
          //item.castShadow = true;
          //item.receiveShadow = true;
          //item.material.shadowSide = THREE.BackSide;
        });
      });
    });
  },

  applyBlobAsTexture: (renderer, blob, item, scene, camera, cb) => {
    var texture = new THREE.Texture();
    var image = new Image();
    image.src = blob;
    image.onload = function () {
      texture.image = image;
      texture.needsUpdate = true;
      item.material.map = texture;
      item.material.needsUpdate = true;
      renderer.render(scene, camera);

      if (typeof cb === "function") cb();
    };
  },

  loadFbx: (scene, fbxUrl, materialUrl, cb, onLoadBegin, onLoadComplete) => {
    let item;
    const manager = new THREE.LoadingManager();
    manager.onStart = function (url, itemsLoaded, itemsTotal) {
      /*console.log(
        "Started loading file: " +
          url +
          ".\nLoaded " +
          itemsLoaded +
          " of " +
          itemsTotal +
          " files."
      );*/
      onLoadBegin();
    };

    //manager.onLoad = onLoadComplete();

    manager.onProgress = function (url, itemsLoaded, itemsTotal) {
      /*console.log(
        "Loading file: " +
          url +
          ".\nLoaded " +
          itemsLoaded +
          " of " +
          itemsTotal +
          " files."
      );*/
      if ((itemsLoaded = itemsTotal)) onLoadComplete();
    };

    manager.onError = function (url) {
      console.log("There was an error loading " + url);
    };

    const loader = new FBXLoader(manager);
    loader.load(fbxUrl, (object) => {
      var textureLoader = new THREE.TextureLoader();
      textureLoader.setCrossOrigin("anonymous");
      textureLoader.load(`${materialUrl}/normal.png`, (normal) => {
        textureLoader.load(`${materialUrl}/roughness.png`, (roughness) => {
          textureLoader.load(`${materialUrl}/base_color.png`, (texture) => {
            // mesh is a group contains multiple sub-objects. Traverse and apply texture to all.
            object.traverse((child) => {
              if (child instanceof THREE.Mesh) {
                item = child;
                // apply texture
                child.material.side = THREE.DoubleSide;
                child.material.normalMap = normal;
                child.material.roughnessMap = roughness;
                child.material.map = texture;
                child.material.needsUpdate = true;
                //child.castShadow = true;
                child.receiveShadow = true;
                child.material.shadowSide = THREE.BackSide;
              }
            });
            if (typeof cb === "function") cb(item);
          });
        });
      });
      scene.add(object);
    });
  },

  fitCameraToObject: ({ camera, selection, controls }) => {
    //https://discourse.threejs.org/t/camera-zoom-to-fit-object/936/23
    if (!selection) return;

    const sel = !Array.isArray(selection) ? [selection] : selection;

    const box = new THREE.Box3();
    for (const obj of sel) box.expandByObject(obj);

    var bb = new THREE.Box3();
    bb.setFromObject(selection);
    bb.getCenter(controls.target);

    const size = box.getSize(new THREE.Vector3());
    const center = box.getCenter(new THREE.Vector3());

    const maxSize = Math.max(size.x, size.y, size.z);
    const fitHeightDistance =
      maxSize / (2 * Math.atan((Math.PI * camera.fov) / 360));
    const fitWidthDistance = fitHeightDistance / camera.aspect;
    const distance = 0.18 * Math.max(fitHeightDistance, fitWidthDistance);

    const direction = controls.target
      .clone()
      .sub(camera.position)
      .normalize()
      .multiplyScalar(distance);

    controls.maxDistance = distance * 10;
    controls.target.copy(center);

    camera.near = distance / 100;
    camera.far = distance * 100;
    camera.updateProjectionMatrix();

    camera.position.copy(controls.target).sub(direction);

    controls.update();
  },
};
export default fbxViewer;

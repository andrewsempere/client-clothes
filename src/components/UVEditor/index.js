import React, { useRef, useState } from "react";
import { Stage, Layer, Star, Text, Line } from "react-konva";
import URLImage from "./URLImage";
import Iconify from "components/Iconify";
import { HuePicker } from "react-color";
import FontPicker from "font-picker-react";

function generateShapes(width, height) {
  return [...Array(10)].map((_, i) => ({
    id: i.toString(),
    x: Math.random() * width,
    y: Math.random() * height,
    rotation: Math.random() * 180,
    isDragging: false,
  }));
}
const UVEditor = ({
  uv,
  size,
  onApply,
  rotation,
  targetArea,
  style,
  onClick,
  isActive,
  controlsMain,
  editorSize,
  textureSize,
}) => {
  const [color, setColor] = React.useState("#000000");
  const [lines, setLines] = React.useState([]);
  const [activeFontFamily, setActiveFontFamily] = React.useState();
  const isDrawing = React.useRef(false);
  const [tool, setTool] = useState("pen");
  const [tabIndex, setTabIndex] = useState();
  const { width, height } = editorSize;
  const INITIAL_STATE = []; //generateShapes(width, height);
  const [stars, setStars] = React.useState(INITIAL_STATE);
  const layer = useRef(null);
  const styles = {
    ...style,
    theme_panel: {
      ...style.theme_panel,
      padding: "1rem",
      borderRadius: "0.2rem 0 0 0.2rem",
    },
    theme_editor: {
      ...style.theme_editor,
      width,
      height,
      margin: "auto",
    },
    tabItem: {
      display: "flex",
      alignItems: "center",
      textAlign: "center",
      marginRight: ".5rem",
    },
  };

  const handleMouseDown = (e) => {
    isDrawing.current = true;
    const pos = e.target.getStage().getPointerPosition();
    setLines([...lines, { tool, points: [pos.x, pos.y] }]);
  };

  const handleMouseMove = (e) => {
    if (tabIndex !== 1) return;
    // no drawing - skipping
    if (!isDrawing.current) {
      return;
    }
    const stage = e.target.getStage();
    const point = stage.getPointerPosition();
    let lastLine = lines[lines.length - 1];
    // add point
    lastLine.points = lastLine.points.concat([point.x, point.y]);

    // replace last
    lines.splice(lines.length - 1, 1, lastLine);
    setLines(lines.concat());
  };

  const handleMouseUp = () => {
    isDrawing.current = false;
  };

  const handleDragStart = (e) => {
    const id = e.target.id();
    setStars(
      stars.map((star) => {
        return {
          ...star,
          isDragging: star.id === id,
        };
      })
    );
  };
  const handleDragEnd = (e) => {
    setStars(
      stars.map((star) => {
        return {
          ...star,
          isDragging: false,
        };
      })
    );
  };

  let controls;
  switch (tabIndex) {
    case 0:
    default:
      controls = controlsMain;
      break;
    case 1:
      controls = (
        <div>
          <select
            value={tool}
            onChange={(e) => {
              setTool(e.target.value);
            }}
          >
            <option value="pen">Pen</option>
            <option value="eraser">Eraser</option>
          </select>
          <HuePicker color={color} onChange={(e) => setColor(e.hex)} />
        </div>
      );
      break;
    case 2:
      controls = (
        <div style={{ display: "flex", flexDirection: "row" }}>
          <HuePicker
            color={color}
            onChange={(e) => setColor(e.hex)}
            width={"10rem"}
          />
          <FontPicker
            families={[
              "Lobster",
              "Sedgwick Ave Display",
              "Ruslan Display",
              "Ephesis",
              "Akronim",
              "Staatliches",
              "Pacifico",
              "Indie Flower",
              "Gluten",
              "Caveat",
              "Rubik Beastly",
              "Alpha Slab One",
              "Special Elite",
              "Press Start 2P",
              "Sigmar One",
              "Bangers",
              "Monoton",
              "Bungee",
              "Holtwood One SC",
              "Rampart One",
              "Creepster",
              "Frijole",
              "Codystar",
              "Meddon",
              "Faster One",
            ]}
            apiKey={window._env_.REACT_APP_GOOGLE_FONTS_API_KEY}
            activeFontFamily={activeFontFamily}
            onChange={(font) => setActiveFontFamily(font.family)}
          />
          <div className="apply-font">Hello!</div>
        </div>
      );
      break;
  }

  return (
    <div
      style={styles.theme_panel}
      onMouseDown={onClick}
      onTouchStart={onClick}
    >
      <div style={{ ...styles.theme_editor, overflow: "clip" }}>
        <Stage
          width={editorSize.width}
          height={editorSize.height}
          onMouseDown={handleMouseDown}
          onMousemove={handleMouseMove}
          onMouseup={handleMouseUp}
        >
          <Layer>
            <URLImage
              src={uv}
              size={editorSize}
              rotation={rotation}
              targetArea={targetArea}
            />
          </Layer>
          <Layer
            width={textureSize.width}
            height={textureSize.height}
            ref={layer}
          >
            <Text
              y={200}
              x={100}
              text="Text"
              fontFamily={activeFontFamily}
              fill={color}
              fontSize="50"
            />
            {lines.map((line, i) => (
              <Line
                key={i}
                points={line.points}
                stroke={color}
                strokeWidth={5}
                tension={0.5}
                lineCap="round"
                globalCompositeOperation={
                  line.tool === "eraser" ? "destination-out" : "source-over"
                }
              />
            ))}
            {stars.map((star) => (
              <Star
                key={star.id}
                id={star.id}
                x={star.x}
                y={star.y}
                numPoints={5}
                innerRadius={20}
                outerRadius={40}
                fill="#89b717"
                opacity={0.8}
                draggable
                rotation={star.rotation}
                shadowColor="black"
                shadowBlur={10}
                shadowOpacity={0.6}
                shadowOffsetX={star.isDragging ? 10 : 5}
                shadowOffsetY={star.isDragging ? 10 : 5}
                scaleX={star.isDragging ? 1.2 : 1}
                scaleY={star.isDragging ? 1.2 : 1}
                onDragStart={handleDragStart}
                onDragEnd={handleDragEnd}
              />
            ))}
          </Layer>
        </Stage>
      </div>
      <div style={styles.theme_controls}>{controls}</div>
      <div style={styles.theme_controls}>
        <div
          style={{
            ...styles.tabItem,
            background: tabIndex === 0 ? styles.theme_highlight : "",
          }}
          onClick={() => setTabIndex(0)}
        >
          <Iconify icon="controls-editor-settings" />
        </div>
        <div
          style={{
            ...styles.tabItem,
            background: tabIndex === 1 ? styles.theme_highlight : "",
          }}
          onClick={() => setTabIndex(1)}
        >
          <Iconify icon="controls-editor-edit" />
        </div>
        <div
          style={{
            ...styles.tabItem,
            background: tabIndex === 2 ? styles.theme_highlight : "",
          }}
          onClick={() => setTabIndex(2)}
        >
          <Iconify icon="controls-editor-type-small-caps" />
        </div>
        <div style={{ flexGrow: 1 }} />
        <Iconify
          icon="documents-document-next"
          size={40}
          onClick={async () => {
            const blob = await (
              await fetch(
                layer.current.getStage().toDataURL({
                  pixelRatio: textureSize.width / editorSize.width,
                })
              )
            ).blob();
            onApply({ blob, rotation, targetArea, uv });
          }}
        />
      </div>
    </div>
  );
};

export default UVEditor;

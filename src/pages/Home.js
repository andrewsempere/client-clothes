import React from "react";

import Designer from "components/Designer";

const Home = () => {
  return (
    <div
      style={{
        paddingTop: "5rem",
        backgroundColor: "#121212",
        height: "100vh",
        width: "100vw",
      }}
    >
      <Designer
        style={{
          theme_highlight: "#3700b3",
          theme_pagePanel: {
            margin: "auto",
            backgroundColor: "rgb(255 255 255 / 9%)",
            borderRadius: "0.2rem",
          },
          theme_editor: {
            borderRadius: "0.2rem",
            width: "100%",
            height: "100%",
            background: "black",
          },
          theme_panel: {
            borderRadius: "0.2rem",
            padding: "1rem",
          },
          theme_controls: {
            margin: ".2rem",
            padding: "0rem",
            height: "2.25rem",
            textAlign: "left",
            display: "flex",
            alignItems: "center",
          },
        }}
      />
    </div>
  );
};

export default Home;
